import React from 'react';
import { Button } from 'antd';

class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <Button>按钮</Button>
        <h2>It is {new Date().toLocaleTimeString()}.</h2>
      </div>
    )
  }
}

export default App