let ipUrl = 'http://127.0.0.1:7001/admin/'

let servicePath = {
  getArticleList: ipUrl + 'getArticleList', //  首页文章列表接口
  getArticleById: ipUrl + 'getArticleById/', // 文章详细页内容接口 ,需要接收参数
  getTypeInfo: ipUrl + 'getTypeInfo', // 分类列表
  getListById: ipUrl + 'getListById/', // 文章文磊列表
  checkLogin: ipUrl + 'checkLogin', //  检查用户名密码是否正确
  addArticle: ipUrl + 'addArticle', // 添加文章
  updateArticle: ipUrl + 'updateArticle', // 修改文章
  delArticle: ipUrl + 'delArticle/' ,  //  删除文章
}
export default servicePath
