import React from 'react'
import ReactDOM from 'react-dom'
import 'antd/dist/antd.css'
import Router from './pages/Main'

ReactDOM.render(<Router />, document.getElementById('root'))
